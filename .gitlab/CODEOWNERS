#####
# Quick intro to Codeowners
#
# The format is:
# [section]
# /<directory> <gitlab-user>
#
# The file is organized in to named sections denoting an internal organization function. e.g. People Group, Engineering, etc.
#
# ^[section] - Denotes an optional set of CodeOwners
#              No ^ means one of these Codeowners are required to approve MRs for this section
#
# For more information see https://docs.gitlab.com/ee/user/project/codeowners/
#
# Special notes:  Due to the current implementation in our code base sections can't have spaces in their names.
#                 Sections can't have named codeowners e.g. [section] @codeowner
#
#####

# Please make sure that @gitlab-com/handbook and  @gitlab-com/egroup groups are part of all
# entires where the codeowners are required.  Thats any entry where the group `[]` isn't prefixed
# with a Caret `^`.

# This optional entry is for when no codeowner is found.
^[content-sites]
* @gitlab-com/egroup @gitlab-com/handbook

^[handbook]
/content/handbook/ @sytses @streas @gitlab-com/egroup

# The following entries are all sections which require codeowner approval
# either because they relate to the function of the Handbook or because
# they are a controlled Document.

[CODEOWNERS]
/.gitlab/ @gitlab-com/ceo-chief-of-staff-team @gitlab-com/handbook @gitlab-com/egroup

# The following entries relate to the operation of the handbook repo or the handbook site itself.
[handbook-operations]
/.gitignore @gitlab-com/handbook
/.gitlab-ci.yml @gitlab-com/handbook
/.markdownlint-cli2.jsonc @gitlab-com/handbook
/.markdownlint.yaml @gitlab-com/handbook
/LICENSE @gitlab-com/handbook
/README.md @gitlab-com/handbook
/CONTRIBUTING.md @gitlab-com/handbook
/go.sum @gitlab-com/handbook
/go.mod @gitlab-com/handbook
/package-lock.json @gitlab-com/handbook
/package.json @gitlab-com/handbook
/config/ @gitlab-com/handbook
/layouts/ @gitlab-com/handbook
/scripts/ @gitlab-com/handbook

# handbook content
[handbook-operations-content]
/content/handbook/about/ @gitlab-com/content-sites/handbook-backend @gitlab-com/handbook
/content/handbook/content-websites/ @gitlab-com/content-sites/handbook-backend @gitlab-com/handbook

[handbook-homepage]
/content/_index.html @gitlab-com/handbook
/content/featured-background.png @gitlab-com/handbook
/layouts/home.html @gitlab-com/handbook

[handbook-docs]
/content/docs/ @gitlab-com/handbook @gitlab-com/content-sites @gitlab-com/content-sites/handbook-backend

[ide-configs]
/.editorconfig @gitlab-com/content-sites @gitlab-com/content-sites/handbook-backend
/.nova/ @gitlab-com/content-sites @gitlab-com/content-sites/handbook-backend
/.vscode/ @gitlab-com/content-sites @gitlab-com/content-sites/handbook-backend
/.idea/ @gitlab-com/content-sites @gitlab-com/content-sites/handbook-backend
