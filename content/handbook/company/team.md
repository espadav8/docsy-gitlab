---
title: Meet Our Team
cascade:
  type: docs
---

More than 3,000 people have [contributed to GitLab](http://contributors.gitlab.com/). The GitLab Inc. team consists of
the following {{< team-size >}} team members and their [363+ pets](https://about.gitlab.com/company/team-pets). We
believe we're the world's largest [all-remote organization](https://handbook.gitlab.com/handbook/company/culture/all-remote/)
and we currently have team members in more than 65 countries and regions. This page lists who people report to, and on a
separate page we detail the [organizational structure](https://handbook.gitlab.com/handbook/company/structure/). You can
get a sense of the team culture and our [inclusion](https://handbook.gitlab.com/handbook/company/culture/inclusion/) by
visiting our [culture page](https://handbook.gitlab.com/handbook/company/culture/) and our [Identity Data page](https://handbook.gitlab.com/handbook/company/culture/inclusion/identity-data/).
{.h4}

{{< team >}}
