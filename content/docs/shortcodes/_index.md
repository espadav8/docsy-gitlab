---
title: Shortcodes
---

## Performance Indicator Embeds

**Examples**

### KPI Summary

```md
{{</* performance-indicators/summary org="example" is_key=true /*/>}}
```

{{< performance-indicators/summary org="pi_example" is_key=true >}}

### Regular PI Summary

```md
{{</* performance-indicators/summary org="example" is_key=false /*/>}}
```

{{< performance-indicators/summary org="pi_example" is_key=false >}}

### Key Performance Indicators

```md
{{</* performance-indicators/list org="example" is_key=true /*/>}}
```

{{< performance-indicators/list org="pi_example" is_key=true >}}


### Regular Performance Indicators

```md
{{</* performance-indicators/list org="example" is_key=false /*/>}}
```

{{< performance-indicators/list org="pi_example" is_key=false >}}

## Tableau Embeds

### `tableau`

**Note:** Any other named parameters are added as additional attributes on the `<tableau-viz>` element.

- `src`: URL of visualization to embed
- `height`: default: `400px`
- `toolbar`: `visible | hidden`, default: `hidden`
- `hide-tabs`: `true | false`, default: `false`

**Examples**

```md
{{</* tableau "https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" /*/>}}
```

{{< tableau "https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" />}}

```md
{{</* tableau height="600px" toolbar="visible" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" */>}}
  {{</* tableau/filters "Subtype Label"="bug::vulnerability" /*/>}}
  {{</* tableau/params "Severity Select"="S2" /*/>}}
{{</* /tableau */>}}
```

<!-- markdownlint-disable-next-line MD013 -->
{{< tableau height="600px" toolbar="visible" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" >}}
  {{< tableau/filters "Subtype Label"="bug::vulnerability" >}}
  {{< tableau/params "Severity Select"="S2" >}}
{{< /tableau >}}

```md
{{</* tableau src="https://10az.online.tableau.com/t/gitlab/views/OKR4_7EngKPITest/PastDueInfraDevIssues" */>}}
  {{</* tableau/filters "Subtype Label"="bug::vulnerability" /*/>}}
{{</* /tableau */>}}
```

<!-- markdownlint-disable-next-line MD013 -->
{{< tableau src="https://10az.online.tableau.com/t/gitlab/views/OKR4_7EngKPITest/PastDueInfraDevIssues" >}}
  {{< tableau/filters "Subtype Label"="bug::vulnerability" >}}
{{< /tableau >}}


### `tableau/filters`

Renders a `viz-filter` element for each of the key/value pairs passed as parameters.

```md
{{</* tableau/filters filter1="value1" filter2="value2" */>}}
```

```html
<viz-filter field="filter1" value="value1"></viz-filter>
<viz-filter field="filter2" value="value2"></viz-filter>
```

### `tableau/params`

Renders a `viz-parameter` element for each of the key/value pairs passed as parameters.

```md
{{</* tableau/params param1="value1" param2="value2" */>}}
```

```html
<viz-parameter name="param1" value="value1"></viz-parameter>
<viz-parameter name="param2" value="value2"></viz-parameter>
```

## Tweet Embeds

### `tweet`

- `user`: X/Twitter username
- `id`: tweet ID

**Examples**

```md
{{</* tweet user="sytses" id="1250946875143815168" /*/>}}
```

{{< tweet user="sytses" id="1250946875143815168" >}}
