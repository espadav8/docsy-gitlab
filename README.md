# GitLab Docsy Theme

This theme serves as the base theme for the public and internal handbooks.

## Development

For details on how to develop the theme locally, modify the backend configuration,
add CI/CD and linting jobs, etc., check the handbook development documentation at
<https://handbook.gitlab.com/docs/>
